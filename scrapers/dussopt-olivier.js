var openAgendaScrapper = require('./common/scrap-OpenAgenda');

var calendarId = 'dussopt-olivier';
var openAgendaId = '91242531';

var scrap = function () {
	return openAgendaScrapper.scrap(calendarId, openAgendaId);
}

exports.scrap = scrap;

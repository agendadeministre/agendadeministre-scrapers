var openAgendaScrapper = require('./common/scrap-OpenAgenda');

var calendarId = 'darmanin-gerald';
var openAgendaId = '74271598';

var scrap = function () {
	return openAgendaScrapper.scrap(calendarId, openAgendaId);
}

exports.scrap = scrap;

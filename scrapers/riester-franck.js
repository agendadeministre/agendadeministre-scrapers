var fetch = require('../lib/fetch-n-save');
var fs = require('fs');
var path = require('path');
const parse = require('csv-parse')
var moment = require('moment');
moment.locale('fr');

/*
date,heure,titre,lieu
*/

var scrap = async function () {
	var calendar = [];
	var calendarId = 'riester-franck';
	try {
		var gouv = [];
		var gouvSearch = {};
		var filePath = path.join(process.cwd(), 'data', 'gouv.json');
		const gouvData = await fs.promises.readFile(filePath);
		gouv = JSON.parse(gouvData);
		for (g in gouv) {
			gouvSearch[gouv[g].id] = gouv[g];
		}

		var now = moment();
		if (now.day() == 0) {
			var premierJourDeLaSemaine = now.subtract(1, 'weeks').day(1);
		} else {
			var premierJourDeLaSemaine = now.day(1);
		}
		var weekKey = premierJourDeLaSemaine.format("DDMMYYYY");

		const res = await fetch.nSave('https://www.culture.gouv.fr/agenda/export/csv/164874', calendarId + '-' + weekKey + '.csv');

		var data = await res;

		data = data.replace(/"/g, '');

		parse(data, {
			delimiter: ";",
			comment: '#',
			columns: ['Date de debut', 'Date de fin', 'Nom'],
			from_line: 1
		}, function (err, output) {
			for (i in output) {
				var startDate = moment(output[i]['Date de debut'], "dddd DD MMMM hh:mm");
				var endDate = moment(output[i]['Date de fin'], "dddd DD MMMM hh:mm");
				if (startDate.isValid()) {
					calendar.push({
						id: calendarId + "-" + i,
						calendarId: calendarId,
						title: output[i]['Nom'],
						category: 'time',
						start: startDate.format(),
						end: endDate.format(),
						location: '',
						attendees: [gouvSearch[calendarId].prenom + " " + gouvSearch[calendarId].nom]
					});
				}
			}
			//console.log(calendar);
			var filePath = path.join(process.cwd(), 'data', 'agendas', calendarId + '.json');
			fs.writeFile(filePath, JSON.stringify(Object.values(calendar), null, 2), function (err) {
				if (err) {
					return console.log(calendarId + " " + err);
				} else {
					if (global.env == "dev") {
						console.log("[" + calendarId + "] : " + calendar.length + " events saved !");
					}
				}

			});
		})

	} catch (err) {
		console.error(err);
	}
}

exports.scrap = scrap;

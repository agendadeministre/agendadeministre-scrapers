var fetch = require('../../lib/fetch-n-save');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
moment.locale('fr');

var scrap = async function (calendarId, minId) {
	var calendar = [];
	try {
		var gouv = [];
		var gouvSearch = {};
		var filePath = path.join(process.cwd(), 'data', 'gouv.json');
		const gouvData = await fs.promises.readFile(filePath);
		gouv = JSON.parse(gouvData);
		for (g in gouv) {
			gouvSearch[gouv[g].id] = gouv[g];
		}

		var now = moment();
		if (now.day() == 0) {
			var premierJourDeLaSemaine = now.subtract(1, 'weeks').day(1);
		} else {
			var premierJourDeLaSemaine = now.day(1);
		}
		var weekKey = premierJourDeLaSemaine.format("DDMMYYYY");
		const res = await fetch.nSave('https://www.ecologique-solidaire.gouv.fr/agenda-' + minId, calendarId + '-' + weekKey + '.txt');

		var data = await res;

		const {
			JSDOM
		} = require("jsdom");
		const {
			window
		} = new JSDOM(data);
		const $ = require("jquery")(window);

		var currentDay = "";
		var i = 0;

		$(".content .field__item").children().each(function () {
			if ($(this).prop("tagName") == "H2") {
				currentDay = $(this).text();
			} else {
				if ($(this).find(".schedule").length == 1) {
					var startDate = moment(currentDay + " " + $(this).find(".schedule").first().text(), "dddd D MMMM hh\hmm");
					var endDate = moment(currentDay + " " + $(this).find(".schedule").first().text(), "dddd D MMMM hh\hmm").add(1, 'h');
					$(this).find(".schedule").remove();
					var content = $(this).text().trim();
					calendar.push({
						id: calendarId + "-" + i,
						calendarId: calendarId,
						title: content,
						category: 'time',
						start: startDate.format(),
						end: endDate.format(),
						location: '',
						attendees: [gouvSearch[calendarId].prenom + " " + gouvSearch[calendarId].nom]
					});
				}
				i++;
			}
		});
		//console.log(calendar);

		var filePath = path.join(process.cwd(), 'data', 'agendas', calendarId + '.json');
		fs.writeFile(filePath, JSON.stringify(Object.values(calendar), null, 2), function (err) {
			if (err) {
				return console.log(calendarId + " " + err);
			} else {
				if (global.env == "dev") {
					console.log("[" + calendarId + "] : " + calendar.length + " events saved !");
				}
			}

		});
	} catch (err) {
		console.error(err);
	}
}

exports.scrap = scrap;

var fetch = require('../../lib/fetch-n-save');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
moment.locale('fr');
var buffer = require('buffer').Buffer;
var iconv = require('iconv-lite');

var scrap = async function (calendarId, baseLink) {
	var calendar = [];
	try {
		var gouv = [];
		var gouvSearch = {};
		var filePath = path.join(process.cwd(), 'data', 'gouv.json');
		const gouvData = await fs.promises.readFile(filePath);
		gouv = JSON.parse(gouvData);
		for (g in gouv) {
			gouvSearch[gouv[g].id] = gouv[g];
		}

		const resList = await fetch.simple(baseLink);

		var dataList = await resList;

		const {
			JSDOM
		} = require("jsdom");
		const {
			window
		} = new JSDOM(dataList);
		const $l = require("jquery")(window);

		var currentDay = "";
		var i = -1;
		var total = $l(".une_blck h1 a").length;

		var eachInterval = setInterval(async function () {
			var thisEl = $l(".une_blck h1 a").get(i);
			i++;
			if (total == i || $l(thisEl).attr('href') == "http://www.presse.justice.gouv.fr/garde-des-sceaux-communiques-discours-agenda-10227/agenda-11650/semaine-du-8-avril-2019-32279.html") {
				clearInterval(eachInterval);
				//console.log(calendar);

				var filePath = path.join(process.cwd(), 'data', 'agendas', calendarId + '.json');
				fs.writeFile(filePath, JSON.stringify(Object.values(calendar), null, 2), function (err) {
					if (err) {
						return console.log(calendarId + " " + err);
					} else {
						if (global.env == "dev") {
							console.log("[" + calendarId + "] : " + calendar.length + " events saved !");
						}
					}

				});
			} else {
				const res = await fetch.nSave($l(thisEl).attr('href'), calendarId + '-' + $l(thisEl).attr('href').split('/').pop() + '.txt', {
					encoding: 'binary'
				});

				var data = await res;
				data = iconv.decode(buffer.from(data), 'utf-8');

				const {
					JSDOM
				} = require("jsdom");
				const {
					window
				} = new JSDOM(data);
				const $ = require("jquery")(window);

				var currentDay = "";
				var currentYear = "";
				var j = 0;
				$(".article_blck").children().each(function () {
					if ($(this).prop("tagName") == "H2") {
						var year = $(this).text().split(" ").pop();
						if (year.search(/^\d{4}$/) != -1) {
							currentYear = year;
						}
					} else if ($(this).prop("tagName") == "P" && $(this).children("strong").length > 0) {
						var strong = $(this).children("strong").first().text().trim();
						$(this).children("strong").first().remove();
						if ($(this).text().length == 0) {
							currentDay = strong;
						} else if (strong.search(/^\d{1,2}h\d\d{0,2}$/) != -1) {
							//console.log(strong)
							var startDate = moment(currentDay + " " + currentYear + " " + strong, "dddd D MMMM YYYY hh\hmm");
							var endDate = moment(currentDay + " " + currentYear + " " + strong, "dddd D MMMM YYYY hh\hmm").add(1, 'h');
							if (startDate.isValid() && endDate.isValid()) {
								if (startDate.hour() == 0) {
									//console.log($(this).html())
									//console.log(currentDay)
									//console.log(strong)
								} else {
									var content = $(this).text().trim();
									var location = '';
									if (content && content.split(" - ").length > 1) {
										location = content.split(" - ").pop().trim();
										content = content.replace(" - " + content.split(" - ").pop().trim(), "");
									}
									calendar.push({
										id: calendarId + "-" + j + "-" + i,
										calendarId: calendarId,
										title: content,
										category: 'time',
										start: startDate.format(),
										end: endDate.format(),
										location: location,
										attendees: [gouvSearch[calendarId].prenom + " " + gouvSearch[calendarId].nom]
									});
								}
							}
						} else {
							//console.log($(this).html())
							//console.log(strong)
						}
						j++;
					}
				});
			}
		}, 200);

	} catch (err) {
		console.error(err);
	}
}

exports.scrap = scrap;

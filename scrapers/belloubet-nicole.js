var minJusticeScrapper = require('./common/min-justice');

var calendarId = 'belloubet-nicole';
var baseLink = 'http://www.presse.justice.gouv.fr/garde-des-sceaux-communiques-discours-agenda-10227/agenda-11650/';

var scrap = function () {
	return minJusticeScrapper.scrap(calendarId, baseLink);
}

exports.scrap = scrap;

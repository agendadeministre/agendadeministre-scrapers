var minEcoloSolidaireScrapper = require('./common/min-ecologique-solidaire');

var calendarId = 'poirson-brune';
var minId = 'brune-poirson';

var scrap = () => minEcoloSolidaireScrapper.scrap(calendarId, minId);

exports.scrap = scrap;

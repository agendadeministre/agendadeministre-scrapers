# AgendaDeMinistre

> Vous avez toujours rêvé d'avoir un agenda de ministre ?

## Installation

Clonez :
```
git clone https://framagit.org/DavidLibeau/agendadeministre.git
```

Installez :
```
npm install
```

Il vous faudra peut-être installer browserify.
```
npm install -g browserify
```

Démarrez le projet en local :
```
npm start
```

Et voilà, le serveur est démarré sur `http://localhost:8000` !


Pour la prod, utilisez :
```
npm serv
```



## Contribuer

Toutes contributions sont les bienvenues ! Par exemple, vous pouvez coder des scrappeur.

Voici la liste des agendas des membres du gouvernement à scrapper :

* [X]  M. Emmanuel MARCON
* [X]  M. Edouard PHILIPPE
* [X]  M. Christophe CASTANER
* [X]  Mme Nicole BELLOUBET
* [X]  M. Gérald DARMANIN
* [X]  Mme Florence PARLY
* [X]  M. Bruno LE MAIRE
* [ ]  Mme Muriel PÉNICAUD
* [X]  M. Franck RIESTER
* [X]  Mme Frédérique VIDAL
* [X]  M. Jean-Michel BLANQUER
* [ ]  M. Jean-Yves LE DRIAN
* [ ]  M. Julien DENORMANDIE
* [ ]  M. Didier GUILLAUME
* [X]  Mme Elisabeth BORNE
* [ ]  M. Olivier VÉRAN
* [ ]  Mme Jacqueline GOURAULT
* [ ]  Mme Roxana MARACINEANU
* [ ]  Mme Annick GIRARDIN
* [ ]  M. Sébastien LECORNU
* [ ]  M. Marc FESNEAU
* [ ]  M. Adrien TAQUET
* [X]  Mme Agnès PANNIER-RUNACHER
* [ ]  Mme Amélie DE MONTCHALIN
* [X]  M. Cédric O
* [ ]  Mme Christelle DUBOS
* [X]  Mme Emmanuelle WARGON
* [X]  Mme Brune POIRSON
* [X]  M. Jean-Baptiste DJEBBARI
* [X]  M. Gabriel ATTAL
* [X]  Mme Geneviève DARRIEUSSECQ
* [ ]  M. Jean-Baptiste LEMOYNE
* [X]  M. Laurent NUNEZ
* [ ]  M. Laurent PIETRASZEWSKI
* [ ]  Mme Marlène SCHIAPPA
* [X]  M. Olivier DUSSOPT
* [ ]  Mme Sophie CLUZEL
* [ ]  Mme Sibeth NDIAYE


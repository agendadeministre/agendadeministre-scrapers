var console = require('./log-n-save');
var request = require('request-promise');
const puppeteer = require('puppeteer');
var fs = require('fs');

var nSave = async function (url, filename = undefined, options = {}) {
	options['url'] = url;
	if (filename == undefined) {
		filename = url.split('/').pop();
	}
	try {
		if (global.env == "dev") {
			console.log("fetch(" + url + ")");
		}
		const res = await request(options);
		var data = await res
		
		fs.writeFile('archives/' + filename, JSON.stringify(data),
			function (err) {
				if (err) {
					return console.log(err);
				}

			});

		return new Promise(resolve => {
			resolve(data)
		})
	} catch (err) {
		console.log("Bad response from server (status code : "+err.statusCode+" for url "+url+")");
	}
}

var simple = async function (url, options = {}) {
	options['url'] = url;
	try {
		if (global.env == "dev") {
			console.log("fetch(" + url + ")");
		}
		const res = await request(options);
		var data = await res;

		return new Promise(resolve => {
			resolve(data)
		})
	} catch (err) {
		console.log("Bad response from server (status code : "+err.statusCode+" for url "+url+")");
	}
}

var fakeBrowser_nSave = async function (url, filename = undefined) {
	if (filename == undefined) {
		filename = url.split('/').pop();
	}
	try {
		if (global.env == "dev") {
			console.log("fetch(" + url + ")");
		}
		
		const browser = await puppeteer.launch();
		const page = await browser.newPage();
		var response = await page.goto(url);
		var data = await page.content();
		await browser.close();

		fs.writeFile('archives/' + filename, JSON.stringify(data),
			function (err) {
				if (err) {
					return console.log(err);
				}

			});

		return new Promise(resolve => {
			resolve(data)
		})
	} catch (err) {
		console.log(err);
		await browser.close();
		console.log("Bad response from server (status code : "+response.headers().status+" for url "+url+")");
	}
}

var fakeBrowser_simple = async function (url) {
	try {
		if (global.env == "dev") {
			console.log("fetch(" + url + ")");
		}
		
		const browser = await puppeteer.launch();
		const page = await browser.newPage();
		var response = await page.goto(url);
		var data = await page.content();
		await browser.close();

		return new Promise(resolve => {
			resolve(data)
		})
	} catch (err) {
		console.log(err);
		await browser.close();
		console.log("Bad response from server (status code : "+response.headers().status+" for url "+url+")");
	}
}

exports.nSave = nSave;
exports.simple = simple;
exports.fakeBrowser = {
	nSave : fakeBrowser_nSave,
	simple : fakeBrowser_simple,
};